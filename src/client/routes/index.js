import { Home } from 'containers/Home'
import { Settings } from 'containers/Settings'

export const routes = [
  {
    path: '/',
    exact: true,
    component: Home,
  },
  {
    path: '/settings',
    exact: true,
    component: Settings
  }
]
