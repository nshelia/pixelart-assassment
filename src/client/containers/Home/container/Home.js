import { updateCurrentQuestionAction } from 'containers/Questions/actions'
import { connect } from 'react-redux'

import Home from '../views/Home'

function mapStateToProps(state) {
  return {
    isFinished: state.questions.isFetched && !state.questions.cities[state.questions.currentIndex],
    score: state.questions.score
  }
}

const mapDispatchToProps = {
  updateCurrentQuestionAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Home)