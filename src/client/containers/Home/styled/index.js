import { device } from 'helpers/device'
import { StyledButton } from 'shared/styled/Button'
import styled from 'styled-components';

export const StyledScore = styled.div`
  font-size: 28px;
  color: red;
  left: 0px;
  color: ${props => props.theme.textColorSecondary};
`

export const StyledNext = styled(StyledButton)`
  width: 80px;
  height: 50px;
  font-size: 20px;
  ${device.mobile} {
    font-size: 18px;
    height: 35px;
  }
 `