import styled, { css } from 'styled-components'

export const StyledButton = styled.button`
  outline:none;
  border: 1px solid ${props => props.theme.white};
  border-radius: 3px;
  background: transparent;
  ${props => props.active && css`
    background: ${props => props.theme.primaryColor};
    border-color: ${props => props.theme.primaryColor};
  `}
  color: ${props => props.theme.white};
  display:flex;
  align-items:center;
  justify-content:center;
  cursor: pointer;
  &:hover {
    background: ${props => props.theme.primaryColor};
    border-color: ${props => props.theme.primaryColor};
  }
`

