import { device } from 'helpers/device'
import styled, { css } from 'styled-components';

export const StyledHeadline = styled.div`
  position: relative;
  font-size: 32px;
  color: white;
  margin-top: 50px;
  text-align:center;
  margin-bottom: 30px;
  display:flex;
  justify-content: space-between;
  align-items: center;
  ${device.mobile} {
    font-size: 18px;
    margin-top: 20px;
  }
  ${props => props.secondary && css`
    font-size: 24px;
    color: ${props => props.theme.textColorSecondary};
    margin: 20px 0px;
  `}
`