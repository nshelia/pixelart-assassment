import { device } from 'helpers/device'
import { NavLink } from 'react-router-dom'
import styled from 'styled-components';

export const StyledTabsWrapper = styled.div`
  display:flex;
  width: 100%;
`

export const StyledTabButton = styled(NavLink)`
  width: 50%;
  padding: 20px 0px;
  font-size: 28px;
  color: ${props => props.theme.textColorSecondary};
  &:first-child {
    border-right: 0;
    border-radius: 0px;
  };
  &:last-child {
    border-top-left-radius: 0;
    border-bottom-left-radius: 0;
  }
  text-align:center;
  text-decoration: none;
  background: ${props => props.theme.primaryColor};
  &:hover,&.active {
    background: ${props => props.theme.tabBtnColor};
  }
  ${device.mobile} {
    padding: 10px 0px;
    font-size: 18px; 
  }
`