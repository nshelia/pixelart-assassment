import { connectRouter } from 'connected-react-router'
import questions from 'containers/Questions/reducer'
import { combineReducers } from 'redux'

export default history => combineReducers({
  router: connectRouter(history),
  questions
})
