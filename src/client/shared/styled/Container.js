import { device } from 'helpers/device'
import styled, { css } from 'styled-components'

export const StyledContainer = styled.div`
  margin: 0px auto;
  position: relative;
  ${device.large} {
    padding-right: 100px;
    padding-left: 100px;
  }
  ${device.mobile} {
    padding-right: 17px;
    padding-left: 17px;
  }
`