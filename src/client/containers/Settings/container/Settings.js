import {
  updateWeatherFormatAction
} from 'containers/Questions/actions'
import { connect } from 'react-redux'

import Settings from '../views/Settings'

function mapStateToProps(state) {
  return {
    useMetric: state.questions.useMetric,
    history: state.questions.history
  }
}

const mapDispatchToProps = {
  updateWeatherFormatAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Settings)