import React, { useEffect, useState } from 'react'

import {
  StyledCityItem,
  StyledTemp
} from '../styled'

function CityItem({ item, showTemp, selected, correctCity, onClick }) {

  function getTemp() {
    return item.temp > 0 ? `+${item.temp}` : item.temp
  }

  function getColor() {
    if (!selected) {
      return null
    } else {
      if (selected.chosen !== correctCity && item.name === selected.chosen) {
        return 'red'
      }
      if (selected.chosen === correctCity && item.name === correctCity) {
        return 'green'
      }
    }
  }

  return (
    <StyledCityItem color={getColor()} onClick={onClick}>
      {item.name}
      {showTemp && (
        <StyledTemp>
          {getTemp()}
        </StyledTemp>
      )}
    </StyledCityItem >
  )
}

export default CityItem