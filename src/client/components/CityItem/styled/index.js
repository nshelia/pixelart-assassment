import { device } from 'helpers/device'
import styled, { css, keyframes } from 'styled-components';

export const StyledCityItem = styled.div`
  position: relative;
  padding: 50px;
  margin-bottom: 25px;
  border-radius: 10px;
  font-size: 32px;
  text-align: center;
  color: ${props => props.theme.textColorSecondary};
  background: ${props => props.theme.primaryColor};
  transition: transform 0.5s, background 0.3s;
  cursor: pointer;
  &:active {
    transform: scale(0.9);
  }
  ${props => props.color === 'green' && css`
    background: ${props => props.theme.green};
  `}
  ${props => props.color === 'red' && css`
    background: ${props => props.theme.red};
  `}
  ${device.mobile} {
    padding: 30px;
  }
`

export const fadeIn = keyframes`
  from { opacity: 0};
  to { opacity: 1};
`

export const StyledTemp = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
  margin: auto;
  animation: ${fadeIn} 0.3s forwards;
  ${device.mobile} {
    font-size: 24px;
  }
`