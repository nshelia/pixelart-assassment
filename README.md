# Pixelart Assassment - Hot or not

### Installation

Make sure you have Node (LTS) & Yarn (stable) installed on your machine.

-  Duplicate `.env.example` file and rename it `.env`
-  Run `yarn install`

### Commands

- `yarn lint:js` - Run ESLint on client
- `yarn dev` - Run the client webpack dev server
- `yarn build:client` - Build development assets for client
- `yarn build:client-prod` - Build production assets for client