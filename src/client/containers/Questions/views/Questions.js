import { CityItem } from 'components/CityItem'
import React, { useEffect } from 'react'

import {
  StyledBox,
  StyledInputs
} from '../styled'

function Questions({
  cities,
  isFetching,
  isFetched,
  isFailed,
  currentIndex,
  fetchItems,
  addPointAction,
  inputsDisabled,
  useMetric,
  score,
  logAnswerAction,
  selected
}) {


  useEffect(() => {
    fetchItems(useMetric)
  }, [])

  function checkAnswer(correct, title, chosen) {
    addPointAction(correct)
    logAnswerAction({
      result: correct ? 'WON' : 'WRONG',
      title,
      chosen
    })
  }

  function renderCities() {
    let [first, second] = cities.slice(currentIndex, currentIndex + 2)
    if (first && second) {
      let correctCity = first.temp > second.temp ? first.name : second.name
      let title = first.name + '-' + second.name
      return (
        <StyledInputs disabled={inputsDisabled}>
          <CityItem
            showTemp={inputsDisabled}
            correctCity={correctCity}
            selected={selected}
            onClick={() => checkAnswer(correctCity === first.name, title, first.name)}
            item={first}
          />
          <CityItem
            showTemp={inputsDisabled}
            correctCity={correctCity}
            selected={selected}
            onClick={() => checkAnswer(correctCity === second.name, title, second.name)}
            item={second}
          />
        </StyledInputs>
      )
    } else {
      return (
        <StyledBox>{"You scored"} {score} {"points!"}</StyledBox>
      )
    }

  }

  if (isFetching) {
    return <StyledBox>{"Loading..."}</StyledBox>
  }

  if (isFetched) {
    return (
      <>
        {renderCities()}
      </>
    )
  }

  if (isFailed) {
    return <StyledBox>{"Unexpected error"}</StyledBox>
  }

  return null

}

export default Questions