import { device } from 'helpers/device'
import styled, { css } from 'styled-components';

export const StyledInputs = styled.div`
  ${props => props.disabled && css`
    pointer-events:none;
  `}
`

export const StyledBox = styled.div`
  width: 100%;
  display:flex;
  align-items:center;
  justify-content:center;
  padding: 50px 0px;
  color: ${props => props.theme.white};
  font-size: 32px;
  text-transform: uppercase;
  ${device.mobile} {
    font-size: 18px;
  }
`