import styled from 'styled-components';

export const StyledOption = styled.div`
  display: flex;
  color: white;
  margin-bottom: 10px;
  cursor: pointer;
`

export const StyledHistory = styled.div`
  display:flex;
  flex-direction: column;
  span {
    font-size: 18px;
    color: ${props => props.theme.white};
    margin-bottom: 10px;
  }
`