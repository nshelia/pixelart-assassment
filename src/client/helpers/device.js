export const device = {
  large: '@media screen and (min-width: 767px)',
  mobile: '@media (max-width: 767px)'
}