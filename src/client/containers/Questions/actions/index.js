import api from 'api'
import { createAction } from 'redux-actions'

import ids from './cityids'
import {
  ADD_POINT,
  CLEAR_STORE,
  FETCH_ITEMS_FAILURE,
  FETCH_ITEMS_REQUEST,
  FETCH_ITEMS_SUCCESS,
  LOG_ANSWER,
  UPDATE_CURRENT_QUESTION,
  UPDATE_WEATHER_FORMAT
} from './types'

export const clearStoreAction = createAction(CLEAR_STORE)

export const addPointAction = createAction(ADD_POINT)

export const updateWeatherFormatAction = createAction(UPDATE_WEATHER_FORMAT)
export const updateCurrentQuestionAction = createAction(UPDATE_CURRENT_QUESTION)
export const logAnswerAction = createAction(LOG_ANSWER)

export const fetchItemsSuccessAction = createAction(FETCH_ITEMS_SUCCESS)
export const fetchItemsRequestAction = createAction(FETCH_ITEMS_REQUEST)
export const fetchItemsFailureAction = createAction(FETCH_ITEMS_FAILURE)

export const fetchItems = (useMetric) => async (dispatch) => {
  try {
    dispatch(fetchItemsRequestAction())

    const data = await api.getCities(ids, useMetric ? "metric" : "imperial")

    dispatch(fetchItemsSuccessAction(data))
  } catch (e) {
    console.log(e)
    dispatch(fetchItemsFailureAction())
  }
}
