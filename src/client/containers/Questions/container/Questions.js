import { connect } from 'react-redux'

import {
  addPointAction,
  fetchItems,
  logAnswerAction
} from '../actions'
import Questions from '../views/Questions'

function mapStateToProps(state) {
  return state.questions
}

const mapDispatchToProps = {
  fetchItems,
  addPointAction,
  logAnswerAction
}

export default connect(mapStateToProps, mapDispatchToProps)(Questions)