import Weather from './weather'

function getApiInstance() {
  return new Weather()
}

export default getApiInstance()
