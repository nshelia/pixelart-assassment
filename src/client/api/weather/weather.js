import openweather from './clients/openweather'

export default class Weather {
  async getCities(cities, units) {
    return openweather.get(`/group?id=${cities.join(',')}&units=${units}&appid=${process.env.APP_ID}`)
  }
}
