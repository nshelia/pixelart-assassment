import React from "react";
import { StyledContainer } from 'shared/styled/Container'
import { StyledHeadline } from 'shared/styled/Headline'

import { StyledHistory, StyledOption } from '../styled'

function Settings({ useMetric, history, updateWeatherFormatAction }) {
  return (
    <StyledContainer>
      <StyledHeadline>
        {"Settings"}
      </StyledHeadline>
      <StyledHeadline secondary>
        {"Units"}
      </StyledHeadline>
      <StyledOption onClick={() => updateWeatherFormatAction(true)}>
        <input type="radio" checked={useMetric} value="metric" />
        <label>Celcius</label>
      </StyledOption>
      <StyledOption onClick={() => updateWeatherFormatAction(false)} >
        <input type="radio" checked={!useMetric} value="imperial" />
        <label>Fahrenheit</label>
      </StyledOption>
      <StyledHeadline secondary>
        {"History"}
      </StyledHeadline>
      <StyledHistory>
        {history.map(item => {
          return (
            <span key={item.title}>
              {item.title} - {item.result}
            </span>
          )
        })}
      </StyledHistory>


    </StyledContainer>
  )
}

export default Settings