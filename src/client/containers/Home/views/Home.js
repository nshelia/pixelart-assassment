import { Questions } from 'containers/Questions'
import React from "react";
import { StyledContainer } from 'shared/styled/Container'
import { StyledHeadline } from 'shared/styled/Headline'

import {
  StyledNext,
  StyledScore
} from '../styled'

function Home({ score, isFinished, updateCurrentQuestionAction }) {
  return (
    <StyledContainer>
      <StyledHeadline>
        <StyledScore>
          {"Score: "}{score}
        </StyledScore>
        {"Which city is hotter ?"}
        <StyledNext onClick={() => updateCurrentQuestionAction(isFinished)}>
          {isFinished ? "Retry" : "Next"}
        </StyledNext>
      </StyledHeadline>
      <Questions />
    </StyledContainer>
  )
}

export default Home