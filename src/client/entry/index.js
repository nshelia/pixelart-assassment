import '@babel/polyfill'

import { ConnectedRouter } from 'connected-react-router'
import { Application } from 'containers/Application'
import React from 'react'
import { render } from 'react-dom'
import { Provider } from 'react-redux'
import { common } from 'shared/theme'
import createStore from 'store'
import { ThemeProvider } from 'styled-components'

const { store, history } = createStore()

const root = document.getElementById('appMountPoint')

const App = () => {
  return (
    <ThemeProvider theme={common}>
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Application />
        </ConnectedRouter>
      </Provider>
    </ThemeProvider>
  )
}


render(<App />, root);
