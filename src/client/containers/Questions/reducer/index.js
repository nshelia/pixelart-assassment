import { handleActions } from 'redux-actions'

import {
  ADD_POINT,
  CLEAR_STORE,
  FETCH_ITEMS_FAILURE,
  FETCH_ITEMS_REQUEST,
  FETCH_ITEMS_SUCCESS,
  LOG_ANSWER,
  UPDATE_CURRENT_QUESTION,
  UPDATE_WEATHER_FORMAT,
} from '../actions/types'

const initialState = {
  isFetched: false,
  isFailed: false,
  isFetching: false,
  cities: [],
  currentIndex: 0,
  score: 0,
  useMetric: true,
  history: [],
  inputsDisabled: false,
  selected: null
}

const reducerMap = {
  [FETCH_ITEMS_FAILURE]: (state) => {
    return {
      ...state,
      isFetched: false,
      isFetching: false,
      isFailed: true
    }
  },
  [FETCH_ITEMS_REQUEST]: (state) => {
    return {
      ...state,
      isFetched: false,
      isFetching: true,
      isFailed: false
    }
  },
  [FETCH_ITEMS_SUCCESS]: (state, { payload }) => {
    const items = payload.list.map(item => ({ name: item.name, temp: Math.round(item.main.temp * 10) / 10 }))

    return {
      ...state,
      isFetched: true,
      isFetching: false,
      isFailed: false,
      cities: items
    }
  },
  [ADD_POINT]: (state, { payload }) => {
    return {
      ...state,
      inputsDisabled: true,
      score: payload ? state.score + 1 : state.score
    }
  },
  [UPDATE_CURRENT_QUESTION]: (state, { payload }) => {
    return {
      ...state,
      inputsDisabled: false,
      score: payload ? 0 : state.score,
      currentIndex: payload ? 0 : state.currentIndex + 2,
      selected: null
    }
  },
  [UPDATE_WEATHER_FORMAT]: (state, { payload }) => {
    return {
      ...state,
      inputsDisabled: false,
      score: payload !== state.useMetric ? 0 : state.score,
      currentIndex: payload !== state.useMetric ? 0 : state.currentIndex,
      useMetric: payload
    }
  },
  [LOG_ANSWER]: (state, { payload }) => {
    return {
      ...state,
      history: [...state.history, payload],
      selected: payload
    }
  },
  [CLEAR_STORE]: () => {
    return initialState
  }
}

export default handleActions(reducerMap, initialState)
