import { Tabs } from 'components/Tabs'
import React from 'react'
import { hot } from 'react-hot-loader'
import { Route, Switch } from 'react-router'
import { routes } from 'routes'
import { DeviceContext } from 'shared/context'
import { useDevice } from 'shared/hooks/useDevice'
import { GlobalStyles } from 'shared/styled/GlobalStyles'

function Application() {
  const [device] = useDevice()
  return (
    <>
      <DeviceContext.Provider value={device}>
        <Tabs />
        <Switch>
          {routes.map(route => (
            <Route
              component={route.component}
              exact={route.exact}
              key={`route-${route.path}`}
              path={route.path}
            />
          ))}
        </Switch>
      </DeviceContext.Provider>
      <GlobalStyles />
    </>
  )
}
export default hot(module)(Application)
