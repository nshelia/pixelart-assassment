export const moduleName = 'questions';

export const FETCH_ITEMS_SUCCESS = `${moduleName}/FETCH_ITEMS_SUCCESS`
export const FETCH_ITEMS_REQUEST = `${moduleName}/FETCH_ITEMS_REQUEST`
export const FETCH_ITEMS_FAILURE = `${moduleName}/FETCH_ITEMS_FAILURE`
export const CLEAR_STORE = `${moduleName}/CLEAR_STORE`
export const ADD_POINT = `${moduleName}/ADD_POINT`
export const UPDATE_CURRENT_QUESTION = `${moduleName}/UPDATE_CURRENT_QUESTION`
export const UPDATE_WEATHER_FORMAT = `${moduleName}/UPDATE_WEATHER_FORMAT`
export const LOG_ANSWER = `${moduleName}/LOG_ANSWER`