export const common = {
  secondaryColor: '#304351',
  primaryColor: '#1C68C3',
  textColorPrimary: '#DAE6ED',
  textColorSecondary: '#DAE6ED',
  black: '#000000',
  white: '#FFFFFF',
  yellow: '#EEC924',
  textColorDark: '#CCCCCC',
  tabBtnColor: "#325886",
  green: "#279966",
  red: "#E50000"
}

