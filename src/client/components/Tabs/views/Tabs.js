import React from "react";

import {
  StyledTabButton,
  StyledTabsWrapper
} from '../styled'

function Tabs() {
  return (
    <StyledTabsWrapper>
      <StyledTabButton exact to="/">
        {"QUESTIONS"}
      </StyledTabButton>
      <StyledTabButton exact to="/settings">
        {"SETTINGS"}
      </StyledTabButton>
    </StyledTabsWrapper>
  )
}

export default Tabs